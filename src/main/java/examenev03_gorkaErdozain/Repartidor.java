package examenev03_gorkaErdozain;

/***
 * Clase Repartidor
 * @author gorka
 *
 */
public class Repartidor extends Empleado implements IRepartir {

	private String zona;
	
	public Repartidor(String nombre, String apellido, int edad, double salario, String zona) {
		super(nombre, apellido, edad, salario);
		this.zona = zona;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n" + "Zona: " + this.getZona();
	}

	/**
	 * M�todo para aumentar el salario con el valor dado s� el salario del repartidor no es mayor a 500
	 * En caso de ser cierto, retorna true
	 */
	@Override
	public boolean plus(double valor) {
		if (this.getSalario() < 500) {
			this.setSalario(getSalario() + valor);
			return true;
		}
		return false;
	}
	
	public void pedalear() {
		System.out.println("txirrin-txarran");
	}


	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	
	
}
