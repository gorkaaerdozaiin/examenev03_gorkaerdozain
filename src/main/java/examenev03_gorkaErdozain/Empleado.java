package examenev03_gorkaErdozain;

public abstract class Empleado {

	private String nombre;
	private String apellido;
	private int edad;
	private double salario;

	public Empleado() {
		nombre = "";
		apellido = "";
		edad = 0;
		salario = 0.0;
	}

	public Empleado(String nombre, String apellido, int edad, double salario) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.salario = salario;
	}

	public abstract boolean plus(double valor);

	private boolean compruebaNombre(String nombre) {
		if (this.getNombre().equals(nombre)) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Empleado\n" + "===========" + "\n" + "Nombre: " + this.getNombre() + "\n" + "Apellido: " + this.getApellido()
				+ "\n" + "Edad: " + this.getEdad() + "\n" + "Salario: " + this.getSalario();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

}
