package examenev03_gorkaErdozain;

/***
 * Clase Comercial
 * @author gorka
 *
 */
public class Comercial extends Empleado implements IVender {

	private double comision;

	public Comercial(String nombre, String apellido, int edad, double salario, double comision) {
		super(nombre, apellido, edad, salario);
		this.comision = comision;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n" + "Comisi�n: " + this.getComision();
	}
	
	/**
	 * M�todo para aumentar el salario con el valor dado s� el salario del comercial es menor a 2000
	 * En caso de ser cierto, retorna true
	 */
	@Override
	public boolean plus(double valor) {
		if (this.getSalario() < 2000) {
			this.setSalario(getSalario() + valor);
			return true;
		}
		return false;
	}

	public void convencer() {
		System.out.println("bueno, bonito, barato");
	}
	
	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}


	
	
	
}
