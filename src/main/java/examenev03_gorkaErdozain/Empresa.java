package examenev03_gorkaErdozain;

import java.util.ArrayList;
/***
 * Clase Empresa
 * @author gorka
 *
 */
public class Empresa {
	

	private String nombre;
	private String nif;
	private ArrayList<Empleado>empleados;
	
	public Empresa(String nombre, String nif) {
		this.nombre = nombre;
		this.nif = nif;
		empleados = new ArrayList<Empleado>();
	}
	
	/**
	 * M�todo para a�adir nuevos empleados a la lista.
	 */
	public void addEmpleado(Empleado emp) {
		if (!empleados.contains(emp)) {			
			empleados.add(emp);
		} else {
			System.out.println("El empleado ya est� en la lista");
		}
	}
	
	/**
	 * M�todo para obtener la suma total de los salarios.
	 */
	public double totalSalario() {
		double salarioTotal = 0;
		for (Empleado empleado : empleados) {
			salarioTotal += empleado.getSalario();
		}
		return salarioTotal;
	}
	
	/**
	 * Funci�n encargada de recorrer la lista de empleados a�adiendo la extra mediante la funci�n plus de empleados.
	 * En caso de que retorne true, muestra mensaje en pantalla.
	 */
	public void actualizarExtra(double extra) {
		for (Empleado empleado : empleados) {
			if (empleado.plus(extra)) {
				System.out.println(empleado.getNombre() + empleado.getApellido() + ": extra actualizado");
			}
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}
	
}
