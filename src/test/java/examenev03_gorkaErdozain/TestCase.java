package examenev03_gorkaErdozain;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestCase {

	Comercial gorka;
	Repartidor mario;
	Empresa amazon;

	@BeforeEach
	void setUp() throws Exception {
		gorka = new Comercial("Gorka", "Erdoz�in", 19, 1500d, 200d);
		mario = new Repartidor("Mario", "Ib��ez", 18, 1100d, "Anso�in");
		amazon = new Empresa("Amazon", "11222333h");
		amazon.addEmpleado(mario);
		amazon.addEmpleado(gorka);
	}

	@Test
	void testCompruebaPersona() {
		// 1� opci�n
		assertTrue(gorka.getNombre().equals("Gorka") && gorka.getApellido().equals("Erdoz�in") && gorka.getEdad() == 19
				&& gorka.getSalario() == 1500d && gorka.getComision() == 200d);
		// 2� opci�n
		//Comercial com = new Comercial("Gorka", "Erdoz�in", 19, 1500d, 200d);
		//assertEquals(gorka, com);
	}
	
	@Test
	void testActualizarExtraValidacion() {
		assertTrue(gorka.plus(100) == true);
	}
	
	@Test
	void testTotalSalario() {
		assertTrue(amazon.totalSalario() == gorka.getSalario() + mario.getSalario());
	}
	
	@Test
	void testRepartidorToString() {
		String toStringRepartidor = "Empleado\n" + "===========" + "\n" + "Nombre: " + mario.getNombre() + "\n" + "Apellido: " + mario.getApellido()
		+ "\n" + "Edad: " + mario.getEdad() + "\n" + "Salario: " + mario.getSalario() + "\n" + "Zona: " + mario.getZona();
		assertTrue(mario.toString().equals(toStringRepartidor));
	}

}
